import cProfile
import pstats
from pstats import SortKey
from src.app import util
import bench_util
import matplotlib.pyplot as plt
import numpy as np
from numpy import poly1d as np_poly1d, polyfit as np_polyfit
import math

operation_lst = []
len_array = []

for i in range(10, 100, 10):
    graph = util.inc
    start = 1
    lst = util.shortest_path(graph, start, i)
    temp = 0
    for j in range(5):
        cProfile.run('util.shortest_path(graph, start, i)', 'stats.log')
        with open('../output.txt', 'w') as log_file_stream:
            p = pstats.Stats('stats.log', stream=log_file_stream)
            p.strip_dirs().sort_stats(SortKey.CALLS).print_stats()
        f = open('../output.txt')
        line = bench_util.correct_lines(f)
        f.close()
        temp += int(line)
    operation_lst.append(temp//5)
    len_array.append(i)

teory_operation = list(operation_lst)
np.seterr(divide = 'ignore')
teory_hard = [ i**i/50 for i in range(len(teory_operation))]

plt.title('"Наивный поиск"')

trend = np_poly1d(np_polyfit(len_array, operation_lst, 2))
plt.plot(len_array, operation_lst, c='green', label='Наивный')
plt.plot(len_array, teory_hard, c='red', label='Теор. сложность')
plt.grid()
plt.legend()
plt.xlabel('Сложность алгоритма')
plt.ylabel('Количество операций ')


plt.show()
